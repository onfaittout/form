# Exercice "Onfaittout" - Partie I

Créer un formulaire à l'aide de Google Form pour récupérer des données de voyage. Transmettre les données récupérées en utilisant Google Sheet.

### Lien vers le formulaire GoogleForm "Onfaittout" (infos sur les voyages)

[Google Form - Onfaittout] (https://docs.google.com/forms/d/e/1FAIpQLSfX4OdXeL4nRCnYBxJwhnZfO20NGt3DsprFC9uzjZrlwgcyYQ/viewform)

### Lien vers la Google Sheet "Onfait tout". 

Les données des formulaires sont enregistrées dans la Google Sheet "Onfaittout"

[Google Sheet "Onfaittout"] (https://docs.google.com/spreadsheets/d/13JC_KQ_aZVf9YfoKDuxxoRFjn0HyxvoegTkHAbvIMUI/edit?resourcekey#gid=529122359)

