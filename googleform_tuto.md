## Etape 1

Aller sur Google Forms -> Accéder à Forms

## Etape 2

Accéder au formulaire "Onfaittout"

## Etape 3
Créer les différents champs 
1) Pour le champ "email", choisir "réponse courte", cliquer en bas à droite de la section sur les 3 points verticaux -> "autres options" -> "validation de réponse"
choisir "texte" -> "email". 
Lors de la saisie, un message d'erreur sera affiché si le format de l'email est incorrecte.

2) Pour le champ "Durée", choisir "réponse courte" cliquer en bas à droite de la section sur les 3 points verticaux -> "autres options" -> "validation de réponse". Sélectionner "Nombre" -> "Nombre enier". Lors de la saisie, un nombre entier devra être saisi sinon un message d'erreur sera affiché, 

# Etape 4
Réponses dans GoogleSheets.Dans "Forms" en haut de l'écran, choisir "Réponses" -> cliquer sur les 3 points verticaux, en haut à droite -> choisir "Sélectionner une destination pour les réponses".

# Etape 5
Pour partager la feuille de réponses "Google Sheet", cliquez sur "Partager" -> Ajouter des personnes, entrez l'adresse email de la personne puis cliquer sur "Envoyez"

# Etape 6
Mettre les liens du formulaire Google Forms dans le fichier "Readme.md" du dossier "ONFAITTOUT"


